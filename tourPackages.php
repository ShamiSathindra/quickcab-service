<?php
require 'ride/add_ride.php';

?>

<!DOCTYPE html>
<html>

<head>
    <link type=text/css rel=stylesheet href=css/css_file.css>
    <title>Booking a taxi in Quick Taxi</title>
    <style>
        .container {
            background-image: url(./bookingImages/anuradhapura.jpg);
            background-size: cover;
            color: black;
            float: center;
            border-style: none;
            width: fit-content;
        }
        
        body {
            font-family: 'Comic Sans MS';
            font-weight: bold;
        }
    </style>
    <script>
        function checkForm() {
            if (document.fm.firstname.value == "") {
                alert("please input the First Name.");
                return false;
            }
            if (document.fm.email.value == "") {
                alert("please input the Email.");
                return false;
            }
            if (document.fm.number.value == "") {
                alert("please input the Telephone Number.");
                return false;
            }
            if (document.fm.from.value == "") {
                alert("please input the Pickup location.");
                return false;
            }
            if (document.fm.to.value == "") {
                alert("please input the Drop Location.");
                return false;
            }
            return true;
        }
    </script>

</head>

<body>
<div class="header">
	<img src = ./backgrounds/logo2.png height=130px width= 250px align="left" >
		<div class="topnav">
		<a href= "menu.html"><img src = ./backgrounds/img2.png height=20px width=30px></a>
		<a href= "aboutUs.html"> ABOUT US </a>
		<a href= "ride/ride.php"> RIDE</a>
		<a href="driver/drive.php"> DRIVE</a>
		
		</div>
	</div>

    <center>
        <div class="container">
            <h1>Where do You want to go?</h1>
            <h2>Sign Up With Quick Taxi to book a taxi</h2>

            <table class="style">
                <form action='' method ='POST'>
				<table>
	<tr>
						<td>Pick_up Location</td>
						<td>Drop Location</td>
					</tr>
	<tr>
						<td><input type="text" name="Pick_up_location"></td>
						<td><input type="text" name="drop_location"></td>	
					</tr>
					<tr>
						<td>Pick_up Date</td>
						<td>Pick_up Time</td>
					</tr>
					<tr>
						<td><input type="date" name="date"></td>
						<td><input type="time" name="time"></td>
					</tr>
					<tr>
						<td>Select the Vehicle</td>
					</tr>
					<tr>
						<td><input type="radio" name="vehicle" value="bike" ><img src=./vehicle/bike.png height="75px" width="75px"> </td>
						<td><input type="radio" name="vehicle" value="tuk"><img src=./vehicle/tuk.png height="75px" width="75px"></td>
						<td><input type="radio" name="vehicle" value="car"><img src=./vehicle/car.png height="75px" width="75px"></td>
						<td><input type="radio" name="vehicle" value="van"><img src=./vehicle/van.png height="75px" width="75px"></td>
					</tr>
					<tr>
						<td><input placeholder="Email" type="email" name="email"></td>
						<td><input placeholder="Contact" type="tel" name="phone" pattern="[0-9]{3}-[0-9]{7}"></td>
					</tr>
					<tr><br>
						<td><input name="Reset" type="reset"></td>
						<td><input name="Submit" type="submit" name="submit"></td>
					</tr>
	
</table>
				</form>
        </div>
    </center>
<div class="footer">
			
			
				<ul align="center" >
					<li><a  href="terms.html">T & C</a></li>
					<li><a  href="privacy.html">Privacy Pollicy</a></li>
					<li><a  href="contact.php">Contact Us</a></li>
				</ul>
			
				<table align="right" padding=30px>
				<tr>
					<td><a  href="facebook"><img src=./backgrounds/fb.png height="35px" width="35px" align = "center"></a></td>
					<td><a  href="facebook"><img src=./backgrounds/twitter.png height="35px" width="35px"align = "center"></a></td>
					<td><a  href="facebook"><img src=./backgrounds/insta.png height="35px" width="35px" align = "right"align = "center"></a></td>
				</tr>
			</div>
			<div>
				<ul><p align="center" color="#ffffff">&copy|2019 Name</p></ul>
			</div>

</body>

</html>